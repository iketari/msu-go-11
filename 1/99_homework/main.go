package main

import (
	"sort"
	"strconv"
	"strings"
)

// ReturnInt return int
func ReturnInt() int {
	value := 1

	return value
}

// ReturnFloat return float32
func ReturnFloat() float32 {
	var value float32 = 1.1

	return value
}

// ReturnIntArray return array of int
func ReturnIntArray() [3]int {
	value := [3]int{1, 3, 4}

	return value
}

// ReturnIntSlice return array of int
func ReturnIntSlice() []int {
	value := []int{1, 2, 3}

	return value
}

// IntSliceToString return int array
func IntSliceToString(intSlice []int) string {
	valuesText := []string{}

	for _, number := range intSlice {
		text := strconv.Itoa(number)
		valuesText = append(valuesText, text)
	}

	return strings.Join(valuesText, "")
}

// MergeSlices return int array
func MergeSlices(sliceA []float32, sliceB []int32) []int {
	sliceInt := make([]int, len(sliceA)+len(sliceB))

	for index, floatNum := range sliceA {
		sliceInt[index] = int(floatNum)
	}

	for index, floatNum := range sliceB {
		sliceInt[len(sliceA)+index] = int(floatNum)
	}

	return sliceInt

}

// GetMapValuesSortedByKey return int array
func GetMapValuesSortedByKey(input map[int]string) []string {
	var keys []int
	var result []string

	for key := range input {
		keys = append(keys, key)
	}

	sort.Ints(keys)

	for _, sortedKey := range keys {
		result = append(result, input[sortedKey])
	}

	return result
}
